package gr.hellobl.lucky;

import com.google.gson.annotations.SerializedName;

public class GameModel extends BaseModel {

    @SerializedName("data")
    String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
