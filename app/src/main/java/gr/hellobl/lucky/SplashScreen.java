package gr.hellobl.lucky;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.HashMap;

import retrofit2.Call;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        TextView txtCodeIdentifier = findViewById(R.id.txtCodeIdentifier);
        txtCodeIdentifier.setText(getDeviceUniqueID());

        getGame();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    // release listener in onStop
    @Override
    public void onStop() {
        super.onStop();
    }

    private void getGame() {

        HashMap<String, Object> param = new HashMap<>();
        param.put(Parameter.sessionKey, getDeviceUniqueID());


        NetworkCall.getInstance().getGame(param, new IResponseCallback<GameModel>() {
            @Override
            public void success(GameModel data) {
                Log.d("API ERROR", data.getData());

                SharedPreferences prefs = getApplication().getSharedPreferences(
                        "Summary", Context.MODE_PRIVATE);
                prefs.edit().putString("App", data.getData()).apply();
                beginGame();
            }

            @Override
            public void onFailure(String errorMessage, int code) {

                TextView textView10 = findViewById(R.id.txtSplashMessage);
                textView10.setText("No games found for this device");
                Log.d("API ERROR", "errro");
            }

            @Override
            public void onError(Call<GameModel> responseCall, Throwable T) {
                Log.d("API ERROR", "errro");
            }
        });
    }

    public String getDeviceUniqueID() {
        String device_unique_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return device_unique_id;
    }

    private void beginGame() {
        Thread background = new Thread() {
            public void run() {
                try {
                    sleep(3000);
                    Bundle bundle = new Bundle();

                    ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetwork = manager.getActiveNetworkInfo();

                    if (null != activeNetwork) {
                        Intent intent = new Intent(getApplicationContext(), ChoiceSelection.class);
                        intent.putExtras(bundle);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(SplashScreen.this, NoInternetActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Catch block", Log.getStackTraceString(e));
                }
            }
        };
        background.start();
    }
}
