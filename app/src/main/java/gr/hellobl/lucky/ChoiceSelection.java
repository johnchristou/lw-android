package gr.hellobl.lucky;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;

import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.onesignal.OneSignal;

import java.util.HashMap;
import java.util.Random;

import retrofit2.Call;

public class ChoiceSelection extends AppCompatActivity {

    private TextView coins2;
    private TextView textView10;
    private TextView txtWins;
    private RelativeLayout rlView10;
    private CardView reedem;
    public SharedPreferences spins, sp;
    private String currentSpins;
    private String id;
    private boolean is_box = false;
    private int color = R.color.colorAccentSw;
    private String message = "START SPINNING";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        sp = getSharedPreferences("Summary", MODE_PRIVATE);
        String app = sp.getString("App", "Schweppes");
        if ("Schweppes".equals(app)) {
            message = "START SPINNING";
            color = R.color.colorAccentSw;
            setContentView(R.layout.activity_choice_selection_sw);
        } else if ("CocaCola".equals(app)) {
            message = "START SPINNING";
            color = R.color.backgroundColorCc;
            setContentView(R.layout.activity_choice_selection_cc);
        } else if ("MagicTables".equals(app)) {
            message = "START SPINNING";
            color = R.color.backgroundColorCc;
            setContentView(R.layout.activity_choice_selection_magic_tables);
        } else if ("CocaColaParties".equals(app)) {
            message = "START SPINNING";
            color = R.color.backgroundColorCc;
            setContentView(R.layout.activity_choice_selection_parties);
        }else if ("Mailos".equals(app)) {
            message = "START SPINNING";
            color = R.color.backgroundColorCc;
            setContentView(R.layout.activity_choice_selection_mailos);
        } else if ("MagicMap".equals(app)) {
            message = "START SPINNING";
            color = R.color.backgroundColorCc;
            setContentView(R.layout.activity_choice_selection_magic_map);
        } else if ("CocaColaBox".equals(app)) {
            message = "START GAME";
            color = R.color.backgroundColorCc;
            is_box = true;
            setContentView(R.layout.activity_choice_selection_cc_box);
        } else {
            message = "START SPINNING";
            color = R.color.backgroundColorCc;
            setContentView(R.layout.activity_choice_selection_cc);
        }
        Log.d("Device", getDeviceUniqueID());


        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isAcceptingText()) {
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        }

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                ConnectivityManager manager = (ConnectivityManager) getApplicationContext()
                        .getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = manager.getActiveNetworkInfo();
                if (null != activeNetwork) {
                    if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                    }
                    if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                    }
                } else {
                    Intent intent = new Intent(ChoiceSelection.this, NoInternetActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    finish();
                }
                handler.postDelayed(this, 1000);
            }
        };


        spins = getSharedPreferences("Spins", MODE_PRIVATE);
        currentSpins = spins.getString("Games", "0");

        textView10 = findViewById(R.id.textView10);
        rlView10 = findViewById(R.id.rlView10);
        reedem = findViewById(R.id.reedem);
        reedem.setVisibility(View.INVISIBLE);

        coins2 = findViewById(R.id.textViewCoins);

        coins2.setText(currentSpins);
        TextView txtCodeIdentifier = findViewById(R.id.txtCodeIdentifier);
        txtCodeIdentifier.setText(getDeviceUniqueID());

        ImageView imageView = findViewById(R.id.imageView11);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ObjectAnimator.ofFloat(v, "rotation", 0, 360).start();
                getSummary();
            }
        });

        Utilities utilities = new Utilities();
        SummaryData sd = utilities.getSummaryData("SummaryData", sp);

        int fullQty = 0;
        int fullGivenQty = 0;

        if (sd != null) {
            for (ProductModel pd : sd.getProducts()) {
                fullGivenQty += pd.getQty_given();
                fullQty += pd.getQty();
            }
        }

        txtWins = findViewById(R.id.textViewCoins1);
        txtWins.setText(String.valueOf(fullGivenQty));

        getSummary();
    }

    private void getSummary() {

        HashMap<String, Object> param = new HashMap<>();
        param.put(Parameter.sessionKey, getDeviceUniqueID());
        param.put(Parameter.counter, currentSpins);
        param.put(Parameter.activation_id, 0);

        Utilities utilities = new Utilities();
        SummaryData sd = utilities.getSummaryData("SummaryData", sp);

        if (sd != null) {
            JsonArray productArray = new JsonArray();
            for (ProductModel pd : sd.getProducts()) {

                JsonObject object = new JsonObject();
                object.addProperty("product_id", pd.getProduct_id());
                object.addProperty("qty_given", pd.getQty_given());
                productArray.add(object);
            }
            param.put(Parameter.products, productArray);
            param.put(Parameter.activation_id, sd.getActivation_id());
        }


        NetworkCall.getInstance().setSummary(param, new IResponseCallback<SummaryModel>() {
            @Override
            public void success(SummaryModel data) {

                Utilities utilities = new Utilities();
                SummaryData sd = utilities.getSummaryData("SummaryData", sp);

                if (sd != null) {
                    if (sd.getActivation_id() != data.getSummaryData().getActivation_id()) {
                        resetGame();
                    }
                }

                utilities.setSummaryData("SummaryData", data.getSummaryData(), sp);
                Log.d("Summary", data.getSummaryData().toString());

                reedem.setVisibility(View.VISIBLE);
                textView10.setText(message);
                rlView10.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), color));
                reedem.setEnabled(true);

            }

            @Override
            public void onFailure(String errorMessage, int code) {
                Log.d("API ERROR", "errro");
                reedem.setVisibility(View.VISIBLE);
                textView10.setText("NO ACTIVATION SET");
                rlView10.setBackgroundColor(Color.GRAY);
                reedem.setEnabled(false);
            }

            @Override
            public void onError(Call<SummaryModel> responseCall, Throwable T) {
                Log.d("API ERROR", "errro");
            }
        });
    }

    private void resetGame() {
        currentSpins = "0";
        coins2.setText(currentSpins);
        txtWins.setText(currentSpins);

        SharedPreferences.Editor spinEdit = spins.edit();
        spinEdit.putString("Games", "0");
        spinEdit.apply();
    }

    public void luckyWheel(View view) {

        if (is_box) {
            Intent openLuckyBox = new Intent(getApplicationContext(), LuckyBoxActivity.class);
            startActivity(openLuckyBox);
        } else {
            Intent openLuckyWheel = new Intent(getApplicationContext(), LuckyWheelActivity.class);
            startActivity(openLuckyWheel);
        }


    }

    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");

        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);
    }

    public String getDeviceUniqueID() {
        String device_unique_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return device_unique_id;
    }
}
