package gr.hellobl.lucky;

public interface NetworkURL
{
    String LIVE_URL = "https://luckyw.brandslab.eu";

    String BASE_URL = LIVE_URL;
    String ROOT_URL = BASE_URL + "/api/";

    String GET_SESSION = "login";
    String GET_PRODUCTS = "get-summary";
    String SET_PRODUCTS = "set-summary";
    String GET_GAME = "get-game";
    String UPDATE_ATTENDANCE = "update-attendance";
}

