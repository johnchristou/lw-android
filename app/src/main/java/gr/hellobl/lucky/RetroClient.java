package gr.hellobl.lucky;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroClient {
    private static final OkHttpClient.Builder builder = new OkHttpClient.Builder()
            .connectTimeout(100, TimeUnit.SECONDS)
            .writeTimeout(100, TimeUnit.SECONDS)
            .readTimeout(100, TimeUnit.SECONDS);


    private static Retrofit getRetrofitInstance() {
        builder.addInterceptor(getInterceptor());
        OkHttpClient client = builder.build();
        return new Retrofit.Builder()
                .baseUrl(NetworkURL.ROOT_URL).client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    static ApiService getApiService() {
        return getRetrofitInstance().create(ApiService.class);
    }


    public static HttpLoggingInterceptor getInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            logging.level(HttpLoggingInterceptor.Level.BODY);
        } else {
            logging.level(HttpLoggingInterceptor.Level.NONE);
        }
        return logging;
    }
}


