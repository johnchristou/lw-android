package gr.hellobl.lucky;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public interface IMethodList {

    void getSummary(HashMap<String, String> param, IResponseCallback<SummaryModel> callback);

    void updateAttendance(HashMap<String, String> param, String sessionKey, IResponseCallback<BaseModel> callback);

    void getGame(HashMap<String, Object> param, IResponseCallback<GameModel> callback);

    void setSummary(HashMap<String, Object> param, IResponseCallback<SummaryModel> callback);

}
