package gr.hellobl.lucky;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Dhara on 08/02/2019.
 */

public class Parameter {

    public static final String sessionKey = "sessionKey";
    public static final String counter = "counter";
    public static final String products = "products";
    public static final String activation_id = "activation_id";

    public static HashMap<String, String> getUpdateAttendance(String counter, String activation_id) {
        HashMap<String, String> param = new HashMap<>();
        param.put("activation_id", activation_id);
        param.put("counter", counter);
        return param;
    }
}
