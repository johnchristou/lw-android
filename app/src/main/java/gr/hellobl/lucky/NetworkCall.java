package gr.hellobl.lucky;


import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetworkCall implements IMethodList {

    private static NetworkCall instance = new NetworkCall();
    private ApiService apiService;

    private NetworkCall() {
        apiService = RetroClient.getApiService();
    }

    public static NetworkCall getInstance() {
        return instance;
    }

    public static RequestBody createPartFromString(String text) {
        if (text == null)
            text = "";
        return RequestBody.create(okhttp3.MediaType.parse("text/plain"), text);
    }

    /**
     * get all products
     */
    @Override
    public void getSummary(HashMap<String, String> param, final IResponseCallback<SummaryModel> callback) {
        apiService.getSummary(param).enqueue(new Callback<SummaryModel>() {
            @Override
            public void onResponse(@NotNull Call<SummaryModel> call, @NotNull Response<SummaryModel> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    callback.onFailure(response.message(), response.code());
                }
            }

            @Override
            public void onFailure(@NotNull Call<SummaryModel> call, @NotNull Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    /**
     * get all products
     */
    @Override
    public void setSummary(HashMap<String, Object> param, final IResponseCallback<SummaryModel> callback) {
        apiService.setSummary(param).enqueue(new Callback<SummaryModel>() {
            @Override
            public void onResponse(@NotNull Call<SummaryModel> call, @NotNull Response<SummaryModel> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    callback.onFailure(response.message(), response.code());
                }
            }

            @Override
            public void onFailure(@NotNull Call<SummaryModel> call, @NotNull Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    /**
     * get all products
     */
    @Override
    public void getGame(HashMap<String, Object> param, final IResponseCallback<GameModel> callback) {
        apiService.getGame(param).enqueue(new Callback<GameModel>() {
            @Override
            public void onResponse(@NotNull Call<GameModel> call, @NotNull Response<GameModel> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    callback.onFailure(response.message(), response.code());
                }
            }

            @Override
            public void onFailure(@NotNull Call<GameModel> call, @NotNull Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    @Override
    public void updateAttendance(HashMap<String, String> param, String sessionKey, final IResponseCallback<BaseModel> callback) {
        apiService.updateAttendance(param, sessionKey).enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(@NotNull Call<BaseModel> call, @NotNull Response<BaseModel> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    callback.onFailure(response.message(), response.code());
                }
            }

            @Override
            public void onFailure(@NotNull Call<BaseModel> call, @NotNull Throwable t) {
                callback.onError(call, t);
            }
        });
    }
}