package gr.hellobl.lucky;

public interface ResponseStatus {

    String RESPONSE_SUCCESS = "success";
    String RESPONSE_FAILED = "failed";
}
