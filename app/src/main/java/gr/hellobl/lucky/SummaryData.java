package gr.hellobl.lucky;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SummaryData {

    private int activation_id;
    private int population;
    private List<ProductModel> products;

    public int getActivation_id() {
        return activation_id;
    }

    public void setActivation_id(int activation_id) {
        this.activation_id = activation_id;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public List<ProductModel> getProducts() {
        return products;
    }

    public void setProducts(List<ProductModel> products) {
        this.products = products;
    }
}
