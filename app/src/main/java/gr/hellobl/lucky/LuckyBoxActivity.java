package gr.hellobl.lucky;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import retrofit2.Call;
import rubikstudio.library.LuckyWheelView;
import rubikstudio.library.model.LuckyItem;

public class LuckyBoxActivity extends AppCompatActivity {
    private Calendar calendar;
    private int weekday;
    private TextView txtPrizeResult;
    private String todayString;
    private SharedPreferences sp;
    List<LuckyItem> data = new ArrayList<>();
    private int coin;
    private int allGames = 0;
    private int allGifts = 0;
    private int colors[] = new int[2];
    private int messages[] = new int[2];
    private LinearLayout luckyBox;
    private SharedPreferences spins;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isAcceptingText()) {
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        }

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);


        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = manager.getActiveNetworkInfo();
                if (null != activeNetwork) {
                    if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                    }
                    if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                    }
                } else {
                    Intent intent = new Intent(LuckyBoxActivity.this, NoInternetActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    finish();
                }
                handler.postDelayed(this, 1000);
            }
        };

        sp = getSharedPreferences("Summary", MODE_PRIVATE);

        colors = new int[]{R.color.Spinwell140Cc, R.color.colorPrimaryCc};
        messages = new int[]{R.drawable.win_cc, R.drawable.lost_cc};
        setContentView(R.layout.activity_box_cc);


        txtPrizeResult = findViewById(R.id.txtPrizeResult);


        ImageView imageView = findViewById(R.id.imageView11);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        final SharedPreferences coins = getSharedPreferences("Rewards", MODE_PRIVATE);
        spins = getSharedPreferences("Spins", MODE_PRIVATE);
        luckyBox = findViewById(R.id.luckyBox);

        txtPrizeResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (areGiftsFinished()) {
                    luckyBox.setVisibility(View.GONE);
                    txtPrizeResult.setVisibility(View.VISIBLE);
                    txtPrizeResult.setEnabled(false);

                    txtPrizeResult.setText("ΤΟ ΠΑΙΧΝΙΔΙ ΤΕΛΕΙΩΣΕ");
                } else {
                    luckyBox.setVisibility(View.VISIBLE);
                    txtPrizeResult.setVisibility(View.GONE);
                }
            }
        });


        allGifts = 0;


        Utilities utilities = new Utilities();
        SummaryData sd = utilities.getSummaryData("SummaryData", sp);

        allGames = sd.getPopulation();

        for (ProductModel pd : sd.getProducts()) {
            allGifts += pd.getQty();
        }


        if (areGiftsFinished()) {
            luckyBox.setVisibility(View.GONE);
            txtPrizeResult.setVisibility(View.VISIBLE);
            txtPrizeResult.setEnabled(false);

            txtPrizeResult.setText("ΤΟ ΠΑΙΧΝΙΔΙ ΤΕΛΕΙΩΣΕ");
        }


        findViewById(R.id.play).setEnabled(true);
        findViewById(R.id.play).setAlpha(1f);
        calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        weekday = calendar.get(Calendar.DAY_OF_WEEK);
        todayString = year + "" + month + "" + day;
        final SharedPreferences spinChecks = getSharedPreferences("SPINCHECK", 0);
        final boolean currentDay = spinChecks.getBoolean(todayString, false);
    }

    public void clickGift(View v) {

        int index = getRandomIndex();
        if (index == 1) {
            coin = 0;
        }
        if (index == 2) {
            coin = 1;
        }
        if (index == 3) {
            coin = 0;
        }
        if (index == 4) {
            coin = 1;
        }
        if (index == 5) {
            coin = 0;
        }
        if (index == 6) {
            coin = 1;
        }
        if (index == 7) {
            coin = 0;
        }
        if (index == 8) {
            coin = 1;
        }


        if (coin == 1) {
            ProductModel prod = getRandomGift();
            txtPrizeResult.setText("ΚΕΡΔΙΣΑΤΕ \n" + prod.getProduct_title());
        } else {
            txtPrizeResult.setText("ΠΡΟΣΠΑΘΗΣΤΕ ΞΑΝΑ");
        }

        int spinCount = Integer.parseInt(spins.getString("Games", "0"));
        spinCount++;
        SharedPreferences.Editor spinEdit = spins.edit();
        spinEdit.putString("Games", String.valueOf(spinCount));
        spinEdit.apply();
        findViewById(R.id.play).setEnabled(true);
        findViewById(R.id.play).setClickable(true);
        findViewById(R.id.play).setAlpha(1f);

        luckyBox.setVisibility(View.GONE);
        txtPrizeResult.setVisibility(View.VISIBLE);

        getSummary(String.valueOf(spinCount));
    }

    private ProductModel getRandomGift() {
        Utilities utilities = new Utilities();
        SummaryData sd = utilities.getSummaryData("SummaryData", sp);

        List<ProductModel> finalProds = new ArrayList<>();
        List<ProductModel> finishedProds = new ArrayList<>();
        for (ProductModel pd : sd.getProducts()) {
            if (pd.getQty_given() < pd.getQty()) {
                finalProds.add(pd);
            } else {
                finishedProds.add(pd);
            }
        }

        int randIndex = new Random().nextInt(finalProds.size());
        ProductModel prod = finalProds.get(randIndex);
        finalProds.get(randIndex).setQty_given(prod.getQty_given() + 1);
        finalProds.addAll(finishedProds);
        sd.setProducts(finalProds);
        utilities.setSummaryData("SummaryData", sd, sp);

        return prod;
    }

    private boolean areGiftsFinished() {
        int fullQty = 0;
        int fullGivenQty = 0;

        Utilities utilities = new Utilities();
        SummaryData sd = utilities.getSummaryData("SummaryData", sp);

        SharedPreferences spins = getSharedPreferences("Spins", MODE_PRIVATE);
        int spinCount = Integer.parseInt(spins.getString("Games", "0"));

        if (sd.getPopulation() == spinCount) {
            return true;
        }

        for (ProductModel pd : sd.getProducts()) {
            fullGivenQty += pd.getQty_given();
            fullQty += pd.getQty();
        }

        if (fullGivenQty < fullQty) {
            return false;
        }


        return true;
    }


    private void getSummary(String currentSpins) {

        HashMap<String, Object> param = new HashMap<>();
        param.put(Parameter.sessionKey, getDeviceUniqueID());
        param.put(Parameter.counter, currentSpins);

        Utilities utilities = new Utilities();
        SummaryData sd = utilities.getSummaryData("SummaryData", sp);

        JsonArray productArray = new JsonArray();
        for (ProductModel pd : sd.getProducts()) {

            JsonObject object = new JsonObject();
            object.addProperty("product_id", pd.getProduct_id());
            object.addProperty("qty_given", pd.getQty_given());
            productArray.add(object);
        }
        param.put(Parameter.products, productArray);
        param.put(Parameter.activation_id, sd.getActivation_id());

        NetworkCall.getInstance().setSummary(param, new IResponseCallback<SummaryModel>() {
            @Override
            public void success(SummaryModel data) {

                Utilities utilities = new Utilities();
                utilities.setSummaryData("SummaryData", data.getSummaryData(), sp);
                Log.d("Summary", data.getSummaryData().toString());
            }

            @Override
            public void onFailure(String errorMessage, int code) {
                Log.d("API ERROR", "errro");
            }

            @Override
            public void onError(Call<SummaryModel> responseCall, Throwable T) {
                Log.d("API ERROR", "errro");
            }
        });
    }

    private int getRandomIndex() {

        int[] arr = new int[]{1, 2, 3, 4, 5, 6, 7, 8};
        int freq[] = new int[arr.length];

        float games = allGames;
        float wins = allGifts;

        float win_ratio = (wins / games) * 100;
        float loss_ratio = (100 - win_ratio);

        int i = 0;
        for (int pos : arr) {
            if (pos % 2 == 0) {
                freq[i] = (int) win_ratio / 4;
            } else {
                freq[i] = (int) loss_ratio / 4;
            }
            i++;
        }

        int n = arr.length;

        return myRand(arr, freq, arr.length);
    }

   /* private int getRandomIndex() {
        int[] ind = new int[]{1, 2, 3, 4, 5, 6, 7, 8};
        int rand = new Random().nextInt(ind.length);
        return ind[rand];
    }*/

    private int getRandomRound() {
        Random rand = new Random();
        return rand.nextInt(10) + 15;
    }

    public String getDeviceUniqueID() {
        String device_unique_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return device_unique_id;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, ChoiceSelection.class);
        startActivity(intent);
        finish();
    }

    // Utility function to find ceiling of r in arr[l..h]
    static int findCeil(int arr[], int r, int l, int h) {
        int mid;
        while (l < h) {
            mid = l + ((h - l) >> 1); // Same as mid = (l+h)/2
            if (r > arr[mid])
                l = mid + 1;
            else
                h = mid;
        }
        return (arr[l] >= r) ? l : -1;
    }

    // The main function that returns a random number
// from arr[] according to distribution array
// defined by freq[]. n is size of arrays.
    static int myRand(int arr[], int freq[], int n) {
        // Create and fill prefix array
        int prefix[] = new int[n], i;
        prefix[0] = freq[0];
        for (i = 1; i < n; ++i)
            prefix[i] = prefix[i - 1] + freq[i];

        // prefix[n-1] is sum of all frequencies.
        // Generate a random number with
        // value from 1 to this sum
        int r = ((int) (Math.random() * (323567)) % prefix[n - 1]) + 1;

        // Find index of ceiling of r in prefix array
        int indexc = findCeil(prefix, r, 0, n - 1);
        return arr[indexc];
    }

}
