package gr.hellobl.lucky;

import com.google.gson.annotations.SerializedName;

public class SummaryModel extends BaseModel {

    @SerializedName("data")
    SummaryData summaryData;

    public SummaryData getSummaryData() {
        return summaryData;
    }

    public void setSummaryData(SummaryData summaryData) {
        this.summaryData = summaryData;
    }
}
