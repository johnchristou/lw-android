package gr.hellobl.lucky;

import retrofit2.Call;

public interface IResponseCallback<T> {
    void success(T data);

    void onFailure(String errorMessage, int code);

    void onError(Call<T> responseCall, Throwable T);
}
