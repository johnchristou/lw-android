package gr.hellobl.lucky;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Streaming;

public interface ApiService {

    @GET(NetworkURL.GET_PRODUCTS)
    Call<SummaryModel> getSummary(@QueryMap HashMap<String, String> param); // DONE

    @POST(NetworkURL.UPDATE_ATTENDANCE)
    @FormUrlEncoded
    Call<BaseModel> updateAttendance(@FieldMap HashMap<String, String> param, @Query(Parameter.sessionKey) String sessionKey);

    @POST(NetworkURL.GET_GAME)
    @FormUrlEncoded
    Call<GameModel> getGame(@FieldMap HashMap<String, Object> param);

    @POST(NetworkURL.SET_PRODUCTS)
    @FormUrlEncoded
    Call<SummaryModel> setSummary(@FieldMap HashMap<String, Object> param);

}

