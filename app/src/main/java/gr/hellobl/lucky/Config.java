package gr.hellobl.lucky;

public interface Config {
    boolean isForDevelopment = true;

    String SW_THEME = "Schweppes";
    String CC_THEME = "CocaCola";

    String BASE_THEME = "Schweppes";
}
