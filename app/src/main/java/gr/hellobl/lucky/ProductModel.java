package gr.hellobl.lucky;

public class ProductModel {
    private int product_id;
    private String product_title;
    private int qty;
    private int qty_given;

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getQty_given() {
        return qty_given;
    }

    public void setQty_given(int qty_given) {
        this.qty_given = qty_given;
    }

    public String getProduct_title() {
        return product_title;
    }

    public void setProduct_title(String product_title) {
        this.product_title = product_title;
    }
}
