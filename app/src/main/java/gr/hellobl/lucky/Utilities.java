package gr.hellobl.lucky;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Dhara on 08/02/2019.
 */

public class Utilities {

    public <T> void setSummaryData(String key, T obj, SharedPreferences setSharedPreferences) {
        Gson gson = new Gson();
        String json = gson.toJson(obj);
        set(key, json, setSharedPreferences);
    }

    public void set(String key, String value, SharedPreferences setSharedPreferences) {
        if (setSharedPreferences != null) {
            SharedPreferences.Editor prefsEditor = setSharedPreferences.edit();
            prefsEditor.putString(key, value);
            prefsEditor.commit();
        }
    }

    public SummaryData getSummaryData(String key, SharedPreferences setSharedPreferences) {
        if (setSharedPreferences != null) {

            Gson gson = new Gson();
            SummaryData summaryData;

            String string = setSharedPreferences.getString(key, null);
            Type type = new TypeToken<SummaryData>() {
            }.getType();
            summaryData = gson.fromJson(string, type);
            return summaryData;
        }
        return null;
    }
}
